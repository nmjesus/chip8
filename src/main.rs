use std::fs::File;
use std::io::Read;
use rand::prelude::*;
use std::time::{Duration, Instant};
use minifb::{Key, Window, WindowOptions};


const STACK_SIZE:u8 = 0x10; // 16
const REGISTERS_SIZE:u8 = 0x10; // 16
const KEYPAD_SIZE:u8 = 0x10; // 16
const WIDTH:u8= 0x40; // 64
const HEIGHT:u8= 0x20; // 32
const MEMORY_SIZE:u16 = 0xFFF; // 4096
const START_ADDR:u16 = 0x200; // 512

struct Display {
    pixels: [u8; WIDTH as usize * HEIGHT as usize]
}

impl Display {
    fn new() -> Display {
        Self {
            pixels: [0; WIDTH as usize * HEIGHT as usize]
        }
    }

    fn dump(&self) {
        for p in 0..self.pixels.len() {
            print!("{}", self.pixels[p]);
            println!("");
        }
    }

    fn cls(&mut self) {
        for p in 0..self.pixels.len() {
            self.pixels[p] = 0;
        }
    }
}

struct Keypad {
    keys: [bool; KEYPAD_SIZE as usize],
    key_down: Option<u8>
}

impl Keypad {
    fn new() -> Keypad {
        Self {
            keys: [false; KEYPAD_SIZE as usize],
            key_down: None,
        }
    }
}

struct VM {
    mem: [u8; MEMORY_SIZE as usize],
    v: [u8; REGISTERS_SIZE as usize],
    i: u16,
    stack: [u16; STACK_SIZE as usize],
    sp: u8,
    pc: u16,
    delay_timer: u8,
    sound_timer: u8,
    keypad: Keypad,
    display: Display,
}

impl VM {
    fn new(display: Display, keypad: Keypad) -> VM {
        let mut vm = Self {
            mem: [0; MEMORY_SIZE as usize],
            v: [0; REGISTERS_SIZE as usize],
            i: 0,
            stack: [0; STACK_SIZE as usize],
            sp: 0,
            pc: START_ADDR,
            delay_timer: 0,
            sound_timer: 0,
            keypad: keypad,
            display: display,
        };

        // font sprite 
        let sprite:[u8; 80] = [
            0xf0, 0x90, 0x90, 0x90, 0xf0, // 0
            0x20, 0x60, 0x20, 0x20, 0x70, // 1
            0xf0, 0x10, 0xf0, 0x80, 0xf0, // 2
            0xf0, 0x10, 0xf0, 0x10, 0xf0, // 3
            0x90, 0x90, 0xf0, 0x10, 0x10, // 4
            0xf0, 0x80, 0xf0, 0x10, 0xf0, // 5
            0xf0, 0x80, 0xf0, 0x90, 0xf0, // 6
            0xf0, 0x10, 0x20, 0x40, 0x40, // 7
            0xf0, 0x90, 0xf0, 0x90, 0xf0, // 8
            0xf0, 0x90, 0xf0, 0x10, 0xf0, // 9
            0xf0, 0x90, 0xf0, 0x90, 0x90, // A
            0xe0, 0x90, 0xe0, 0x90, 0xe0, // B
            0xf0, 0x80, 0x80, 0x80, 0xf0, // C
            0xe0, 0x90, 0x90, 0x90, 0xe0, // D
            0xf0, 0x80, 0xf0, 0x80, 0xf0, // E
            0xf0, 0x80, 0xf0, 0x80, 0x80, // F
        ];

        for i in 0..80 {
            vm.mem[i] = sprite[i];
        }

        vm
    }

    fn load(&mut self, rom: &Vec<u8>) -> Result<bool, &str> {
        if rom.len() > (MEMORY_SIZE - START_ADDR) as usize {
            return Err("Not enough memory to load the rom");
        }

        for i in 0..rom.len() {
            self.mem[START_ADDR as usize + i]  = rom[i];
        }

        Ok(true)
    }

    fn run(&mut self) {
        // two bytes opcode, most significant byte-first
        let high = self.mem[self.pc as usize] as u16;
        let low = self.mem[self.pc as usize + 1] as u16;
        let opcode: u16 = (high << 8) | low;

        let nnn = opcode & 0x0fff;
        let nibble = opcode & 0x000f;
        let x = (high & 0x000f) as u8;
        let y = ((low & 0xf0) >> 4) as u8;
        let kk = (opcode & 0x00ff) as u8;

        // println!("low={}, y={}", low, y);
        // println!("
        //     pc={:#X}
        //     high={:#X}
        //     low={:#X}
        //     opcode={:#X}
        //     nnn={:#X}
        //     nibble={:#X}
        //     x={:#X}
        //     y={:#X}
        //     kk={:#X}",
        //     self.pc,
        //     high,
        //     low,
        //     opcode,
        //     nnn,
        //     nibble,
        //     x,
        //     y,
        //     kk
        // );

        // println!("running: {:#X}", opcode);

        match opcode & 0xF000 {
            0x0000 => {
                match nnn {
                    0xE0 => {
                        self.display.cls();
                        self.pc += 2;
                    },
                    0xEE => {
                        let addr = self.stack[(self.sp - 1) as usize];
                        self.sp -= 1;
                        self.pc = addr;
                    },
                    _ => {
                        println!("Invalid instruction");
                    }
                }
            },
            0x1000 => {
                self.pc = nnn;
            },
            0x2000 => {
                self.stack[self.sp as usize] = self.pc + 2;
                self.sp += 1;
                self.pc = nnn;
            },
            0x3000 => {
                if self.v[x as usize] == kk {
                    self.pc += 4;
                } else {
                    self.pc += 2;
                }
            },
            0x4000 => {
                if self.v[x as usize] != kk {
                    self.pc += 4;
                } else {
                    self.pc += 2;
                }
            },
            0x5000 => {
                println!("0x5000");
                if self.v[x as usize] == self.v[y as usize] {
                    self.pc += 4;
                } else {
                    self.pc += 2;
                }
            },
            0x6000 => {
                self.v[x as usize] = kk;
                self.pc += 2;
            },
            0x7000 => {
                println!("opcode={:#X}, kk={:#X}", opcode, kk);
                self.v[x as usize] = self.v[x as usize].wrapping_add(kk);
                self.pc += 2;
            },
            0x8000 => {
                match nibble {
                    0x0 => {
                        self.v[x as usize] = self.v[y as usize];
                        self.pc += 2;
                    },
                    0x1 => {
                        self.v[x as usize] = self.v[x as usize] | self.v[y as usize];
                        self.pc += 2;
                    },
                    0x2 => {
                        self.v[x as usize] = self.v[x as usize] & self.v[y as usize];
                        self.pc += 2;
                    },
                    0x3 => {
                        self.v[x as usize] = self.v[x as usize] ^ self.v[y as usize];
                        self.pc += 2;
                    },
                    0x4 => {
                        let total = self.v[x as usize] + self.v[y as usize];
                        self.v[x as usize] = total;
                        if total > 0xff {
                            self.v[0xf as usize] = 1;
                        }
                        self.pc += 2;
                    },
                    0x5 => {
                        let vx = self.v[x as usize];
                        let vy = self.v[y as usize];
                        self.v[x as usize] = vx - vy;
                        if vx > vy {
                           self.v[0xf] = 1;
                        } else {
                           self.v[0xf] = 0;
                        }
                        self.pc += 2;
                    },
                    0x6 => {
                        self.v[0xf as usize] = self.v[x as usize] & 0x1;
                        self.v[x as usize] >>= 1;
                        self.pc += 2;
                    }
                    0x7 => {
                        let vx = self.v[x as usize];
                        let vy = self.v[y as usize];
                        self.v[x as usize] = vy - vx;
                        if vy > vx {
                            self.v[0xf] = 1;
                        } else {
                            self.v[0xf] = 0;
                        }
                        self.pc += 2;
                    },
                    0xE => {
                        if self.v[0xf] == 0x1 {
                            self.v[0xf] = 1;
                        } else {
                            self.v[0xf] = 0;
                        }
                        self.v[x as usize] <<= 2;
                        self.pc += 2;
                    },
                    _ => println!("invalid instruction: {:#X}", opcode),
                }
            },
            0x9000 => {
                if self.v[x as usize] != self.v[y as usize] {
                    self.pc += 4;
                } else {
                    self.pc += 2;
                }
            },
            0xA000 => {
                self.i = nnn;
                self.pc += 2;
            },
            0xB000 => {
                self.pc = nnn + self.v[0x0] as u16;
            },
            0xC000 => {
                let mut rng = rand::thread_rng();
                let rnd_number:u8 = rng.gen_range(0, 255);
                self.v[x as usize] = rnd_number | kk;
                self.pc += 2;
            },
            0xD000 => {
                let mut pixel;

                for yAxis in 0..nibble {
                    pixel = self.mem[(self.i + yAxis as u16) as usize];
                    for xAxis in 0..8 {
                        if pixel & (0x80 >> xAxis) != 0 {
                            // fix, overflow multiply
                            // println!("x={}, xAxis={}, y={}, yAxis={}, calc={}", x, xAxis, y, yAxis, (x + xAxis + ((y + yAxis as u8) * 64)));
                            // self.display.pixels[197] = 1;
                            if self.display.pixels[(x as u16 + xAxis as u16 + ((y as u16 + yAxis as u16) * 64) as u16) as usize] == 1 {
                                self.v[0xf] = 1;
                            }
                            // self.display.pixels[(x + xAxis + ((y + yAxis as u8) * 64)) as usize] ^= 1;
                            self.display.pixels[(x as u16 + xAxis as u16 + ((y as u16 + yAxis as u16) * 64) as u16) as usize] ^= 1;
                        }
                    }
                }
                self.pc += 2;
            },
            0xE000 => {
                match kk {
                    0x9e => {
                        if let Some(key) = self.keypad.key_down {
                            if self.v[x as usize] == key {
                                self.pc += 4;
                            } else {
                                self.pc += 2;
                            }
                        }
                    },
                    0xa1 => {
                        if let Some(key) = self.keypad.key_down {
                            if self.v[x as usize] != key {
                                self.pc += 4;
                            } else {
                                self.pc += 2;
                            }
                        }
                    },
                    _ => println!("invalid instruction: {:#X}", opcode),
                }
            },
            0xF000 => {
                match kk {
                    0x07 => {
                        self.v[x as usize] = self.delay_timer;
                        self.pc += 2;
                    },
                    0x0a => {
                        if let Some(key) = self.keypad.key_down {
                            self.v[x as usize] = key;
                            self.pc += 2;
                        }
                    },
                    0x15 => {
                        self.delay_timer = self.v[x as usize];
                        self.pc += 2;
                    },
                    0x18 => {
                        // TODO implement sound timer
                    },
                    0x1e => {
                        self.i += self.v[x as usize] as u16;
                        self.pc += 2;
                    },
                    0x29 => {
                        self.i = self.v[x as usize] as u16 * 5;
                        self.pc += 2;
                    },
                    0x33 => {
                        let vx = self.v[x as usize];
                        self.mem[self.i as usize] = vx / 100;
                        self.mem[self.i as usize + 1] = (vx % 100) / 10;
                        self.mem[self.i as usize + 2] = vx % 10;
                        self.pc += 2;
                    },
                    0x55 => {
                        for i in 0..x + 1 {
                            self.mem[(self.i + i as u16) as usize] = self.v[i as usize];
                        }
                        self.i += x as u16 + 1;
                        self.pc += 2;
                    },
                    0x65 => {
                        for i in 0..x + 1 {
                            self.v[i as usize] = self.mem[(self.i + i as u16) as usize];
                        }
                        self.i = x as u16 + 1;
                        self.pc += 2;
                    }
                    _ => println!("invalid instruction: {:#X}", opcode),
                }
            },
            _ => println!("invalid instruction: {:#X}", opcode),
        }
    }

    fn dump(&self) {
        for i in 0..MEMORY_SIZE {
            print!("{:x}", self.mem[i as usize]);
        }
    }
}

fn main() {
    let rom_name = "TETRIS";

    let display = Display::new();
    let keypad = Keypad::new();
    let mut vm = VM::new(display, keypad);

    let mut file = File::open(format!("roms/{}", rom_name)).unwrap();
    let mut rom = Vec::<u8>::new();
    file.read_to_end(&mut rom).expect("not found");

    vm.load(&rom).unwrap();


    // gfx stuff
    let width = 640;
    let height = 320;
    let mut last_update = Instant::now();
    let mut display_last_update = Instant::now();
    let mut vbuffer: Vec<u32> = vec![0; width * height];
    let mut window = Window::new(
        "Chip8",
        width,
        height,
        WindowOptions::default(),
    ).unwrap_or_else(|e| {
        panic!("{}", e);
    });

    window.limit_update_rate(Some(Duration::from_micros(16600)));
    while window.is_open() && !window.is_key_down(Key::Escape) {
        if Instant::now() - last_update > Duration::from_millis(2) {
            last_update = Instant::now();
            vm.run();
        }

        if Instant::now() - display_last_update > Duration::from_millis(10) {
            // for y in 0..height {
            //     let y_coord = y / 10;
            //     let offset = y * width;
            //     for x in 0..width {
            //         let x_coord = x / 10;
            //         let pixel = vm.display.pixels[(y_coord * WIDTH as usize + x_coord) as usize];
            //         let color_pixel = match pixel {
            //             0 => 0x0,
            //             1 => 0xffffff,
            //             _ => unreachable!(),
            //         };
            //         vbuffer[offset + x] = color_pixel;
            //     }
            // }
            for i in 0..2048 {
                let pixel = vm.display.pixels[i];
                // vbuffer[i] = (0x00ffffff as u32 * pixel as u32) | 0xff000000;
                // vbuffer[i] = 0x0;
            }

            // println!("{:?}", vbuffer);
            window.update_with_buffer(&vbuffer, width, height).unwrap();
            display_last_update = Instant::now();
        }
    }

    // vm.dump();

    // println!("rom: {:?}", rom);
    // println!("key[0]={}", vm.keypad.keys[0]);
    // println!("display={}x{}", vm.display.width, vm.display.height);
}
